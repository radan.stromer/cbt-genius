<form action="inc/proses_soal.php" method="post">
                    <?php 
                    $sql_soal_sudah = mysqli_query($db, "SELECT 
                                                            tb_jawaban_pilgan_temp.id_soal,
                                                            tb_jawaban_pilgan_temp.jawaban,
                                                            tb_soal_pilgan.level_group
                                                         FROM tb_jawaban_pilgan_temp 
                                                         JOIN tb_soal_pilgan ON tb_soal_pilgan.id_pilgan = tb_jawaban_pilgan_temp.id_soal
                                                         WHERE tb_jawaban_pilgan_temp.id_peserta = '".$_SESSION['siswa']."' AND tb_jawaban_pilgan_temp.id_tq = '{$id_tq}'") or die ($db->error);
                    $no_sudah="";
                    $group_sudah = "";
                    $n=1;
                    while($soal_sudah = mysqli_fetch_assoc($sql_soal_sudah)) {
                        if($n>1){
                            $no_sudah .= ","; 
                            $group_sudah .= ",";
                        }       
                        $no_sudah .= $soal_sudah["id_soal"];
                        $group_sudah .= $soal_sudah["level_group"];
                        $jawaban = $soal_sudah["jawaban"];
                        $n++;
                    }
                    
                    $acak = mysqli_query($db, "SELECT 
                                                            acak
                                                         FROM tb_topik_quiz 
                                                         WHERE id_tq = '{$id_tq}'") or die ($db->error);
                    $data_acak = mysqli_fetch_row($acak);
                    //print_r($data_acak[0]); exit;
                    if(!empty($no_sudah)){
                        if(isset($_GET['revisi_soal'])){
                        $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan WHERE id_tq = '$id_tq' AND id_pilgan='".$_GET['revisi_soal']."' limit 1 ") or die ($db->error);
                        }else{
                            if($data_acak[0]=='aktif'){
                                $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan WHERE id_tq = '$id_tq' 
                                    AND id_pilgan NOT IN ({$no_sudah}) 
                                    #AND level_group = '$n'
                                    AND level_group NOT IN ({$group_sudah}) 
                                    ORDER BY rand() limit 1 ") or die ($db->error);
                            }else{
                                $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan WHERE id_tq = '$id_tq' 
                                    AND id_pilgan NOT IN ({$no_sudah}) 
                                    #AND level_group = '$n'
                                    AND level_group NOT IN ({$group_sudah}) 
                                    limit 1 ") or die ($db->error);
                            }
                        }
                    }elseif(isset($_GET['revisi_soal'])){
                        $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan WHERE id_tq = '$id_tq' AND id_pilgan='".$_GET['revisi_soal']."' limit 1 ") or die ($db->error);
                    }
                    else{
                        if($data_acak[0]=='aktif'){
                            $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan 
                                                                  WHERE id_tq = '$id_tq' 
                                                                 # AND level_group = '$n' 
                                                                  ORDER BY rand() limit 1 ") or die ($db->error);
                        }else{
                            $sql_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan 
                                                                  WHERE id_tq = '$id_tq' 
                                                                 # AND level_group = '$n' 
                                                                  limit 1 ") or die ($db->error);
                        }
                    }
                    if(mysqli_num_rows($sql_soal_pilgan) > 0){ ?>
                      
                        <style type="text/css">
                            .nomor, .huruf{
                               display: block;
                               width: 35px;
                               padding: 5px 0;
                               text-align: center;
                               border: 1px solid #ccc;
                               border-radius: 50%;
                               cursor: pointer;
                            }
                            /*Mengatur warna background dan teks nomor soal*/
                            .nomor{
                               background: blue;
                               color: #fff;
                            }
                            /*Menyembunyikan input radio*/
                            input[type=radio]{
                               display: none;
                            }
                            /*Mengganti warna background huruf ketika input radio dicentang*/
                            input[type=radio]:checked ~ .huruf{
                               background: green;
                               color: #fff;
                            }
                            /*Mengatur tampilan pilihan*/
                            .pilihan{
                               margin-bottom: 20px;
                            }
                            .pilihan .teks{
                               padding-top: 10px;
                            }
                            /*Mengatur konten ujian dan nomor ujian*/
                            .konten-ujian, .nomor-ujian{
                               border: 1px solid #eee;
                               padding: 10px;
                            }
                            .blok-soal .box{
                               height: 400px;
                               overflow-y: auto;
                               overflow-x: hidden;
                            }
                            .teks p{
                                font-size: 18px;
                            }
                            /*.blok-soal{
                               display: none;
                            }
                            .blok-soal.active{
                                display: block;
                            }*/
                        </style>
                        <div class="konten-ujian">
                            <div class="blok-soal">
                              <?php  while($data_soal_pilgan = mysqli_fetch_array($sql_soal_pilgan)) { ?>
                                <input type="hidden" name="id_pilgan" value="<?php echo $data_soal_pilgan['id_pilgan']; ?>">
                                <div class="box">
                                    <div class="row">
                                       
                                        <div class="col-xs-1"><div class="nomor"><?php echo (isset($_GET['no_revisi'])?$_GET['no_revisi']:$n); ?></div></div>
                                        <div class="col-xs-11">
                                            <div class="soal" style="font-size: 18px;">
                                                <?php echo $data_soal_pilgan['pertanyaan']; ?>

                                                <?php if($data_soal_pilgan['gambar'] != '') { ?>
                                                    <a class="img-soal" href="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gambar']; ?>">
                                                            <img width="220px" src="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gambar']; ?>" />
                                                    </a>
                                                <?php } ?>

                                                <?php if($data_soal_pilgan['video'] != '') { ?>
                                                     <video width="220px" controls>
                                                            <source src="admin/video/<?php echo $data_soal_pilgan['video']; ?>" type="video/mp4">
                                                            <source src="admin/video/<?php echo $data_soal_pilgan['video']; ?>" type="video/ogg">
                                                            Your browser does not support the video tag.
                                                     </video>
                                                <?php } ?>

                                                <?php if($data_soal_pilgan['audio'] != '') { ?>
                                                    <audio controls>
                                                            <source src="<?php echo URL_ASSETS.'audio_soal_pilgan/'.$data_soal_pilgan['audio']; ?>" type="audio/mpeg">
                                                            <source src="<?php echo URL_ASSETS.'audio_soal_pilgan/'.$data_soal_pilgan['audio']; ?>" type="audio/ogg">
                                                            Your browser does not support the audio tag.
                                                    </audio>
                                                <?php } ?>
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="row pilihan">
                                        <div class="col-md-1 col-xs-2 col-xs-offset-1">
                                            <input type="radio" name="soal_pilgan[<?php echo $data_soal_pilgan['id_pilgan']; ?>]" value="A" id="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_A">
                                            <label class="huruf" for="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_A">A</label>
                                        </div>
                                        <div class="col-md-10 col-xs-9">
                                           <div class="teks">
                                            <?php echo $data_soal_pilgan['pil_a']; ?>
                                            <?php if($data_soal_pilgan['gbr_a'] != '') { ?><br>
                                                <a class="img-soal" href="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_a']; ?>">
                                                    <img width="220px" src="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_a']; ?>" />
                                                </a>
                                            <?php } ?>    
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="row pilihan">
                                        <div class="col-md-1 col-xs-2 col-xs-offset-1">
                                            <input type="radio" name="soal_pilgan[<?php echo $data_soal_pilgan['id_pilgan']; ?>]" value="B" id="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_B">
                                            <label class="huruf" for="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_B">B</label>
                                        </div>
                                        <div class="col-md-10 col-xs-9">
                                           <div class="teks">
                                            <?php echo $data_soal_pilgan['pil_b']; ?>
                                            <?php if($data_soal_pilgan['gbr_b'] != '') { ?><br>
                                                <a class="img-soal" href="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_b']; ?>">
                                                    <img width="220px" src="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_b']; ?>" />
                                                </a>
                                            <?php } ?>    
                                            </div> 
                                        </div>
                                    </div>

                                    <div class="row pilihan">
                                        <div class="col-md-1 col-xs-2 col-xs-offset-1">
                                            <input type="radio" name="soal_pilgan[<?php echo $data_soal_pilgan['id_pilgan']; ?>]" value="C" id="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_C">
                                            <label class="huruf" for="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_C">C</label>
                                        </div>
                                        <div class="col-md-10 col-xs-9">
                                           <div class="teks">
                                            <?php echo $data_soal_pilgan['pil_c']; ?>
                                            <?php if($data_soal_pilgan['gbr_c'] != '') { ?><br>
                                                <a class="img-soal" href="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_c']; ?>">
                                                    <img width="220px" src="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_c']; ?>" />
                                                </a>
                                            <?php } ?>    
                                           </div> 
                                        </div>
                                    </div>

                                    <div class="row pilihan">
                                        <div class="col-md-1 col-xs-2 col-xs-offset-1">
                                            <input type="radio" name="soal_pilgan[<?php echo $data_soal_pilgan['id_pilgan']; ?>]" value="D" id="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_D">
                                            <label class="huruf" for="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_D">D</label>
                                        </div>
                                        <div class="col-md-10 col-xs-9">
                                           <div class="teks">
                                            <?php echo $data_soal_pilgan['pil_d']; ?>
                                            <?php if($data_soal_pilgan['gbr_d'] != '') { ?><br>
                                                <a class="img-soal" href="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_d']; ?>">
                                                    <img width="220px" src="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_d']; ?>" />
                                                </a>
                                            <?php } ?>   
                                           </div> 
                                        </div>
                                    </div>

                                    <div class="row pilihan">
                                        <div class="col-md-1 col-xs-2 col-xs-offset-1">
                                            <input type="radio" name="soal_pilgan[<?php echo $data_soal_pilgan['id_pilgan']; ?>]" value="E" id="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_E">
                                            <label class="huruf" for="soal_pilgan_<?php echo $data_soal_pilgan['id_pilgan']; ?>_E">E</label>
                                        </div>
                                        <div class="col-md-10 col-xs-9">
                                           <div class="teks">
                                            <?php echo $data_soal_pilgan['pil_e']; ?>
                                            <?php if($data_soal_pilgan['gbr_e'] != '') { ?><br>
                                                <a class="img-soal" href="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_e']; ?>">
                                                    <img width="220px" src="<?=URL_ASSETS;?>gambar_soal_pilgan/<?php echo $data_soal_pilgan['gbr_e']; ?>" />
                                                </a>
                                            <?php } ?>     
                                           </div> 
                                        </div>
                                    </div>
                                </div>
                                <input type="submit" value="selanjutnya">
                                <input style="float:right;" class="btn btn-warning" type="submit" value="Ragu-ragu" name="ragu" disabled>
                                <?php }?>
                                <input type="hidden" name="jumlahsoalpilgan" value="<?php echo mysqli_num_rows($sql_soal_pilgan); ?>" />
                                <script type="text/javascript">
                                    $(".huruf").click(function(){
                                        $("input[name='ragu']").removeAttr('disabled');
                                    });
                                </script>
                            </div>
                        </div>
                    <?php } 
                    $sql_soal_essay = mysqli_query($db, "SELECT * FROM tb_soal_essay WHERE id_tq = '$id_tq' ORDER BY rand()") or die ($db->error);
                    if(mysqli_num_rows($sql_soal_essay) > 0) {
                        include "soal_esay.php";
                    } ?>
                    
                    <input type="hidden" name="id_tq" value="<?php echo $id_tq; ?>" />
                    <?php if(mysqli_num_rows($sql_soal_pilgan) == 0) {?>
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <div>
                                <a id="selesai" class="btn btn-info">Selesai</a>
                                <!-- <input type="reset" value="Reset Jawaban" class="btn btn-danger" /> -->
                            </div>
                            <div id="konfirm" style="display:none; margin-top:15px;">
                                Apakah Anda yakin sudah selesai mengerjakan soal dan akan mengirim jawaban? &nbsp; <input onclick="selesai();" type="submit" id="kirim" value="Ya" class="btn btn-info btn-sm" />
                            </div>
                            <script type="text/javascript">
                            $("#selesai").click(function() {
                                $("#konfirm").fadeIn(1000);
                            });
                            </script>
                        </div>
                    </div>
                    <?php }else{?>
                        <input onclick="selesai();" type="submit" id="kirim" value="Ya" class="btn btn-info btn-sm hidden" />
                    <?php }?>
                </form>