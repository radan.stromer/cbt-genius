<?php
$id = @$_GET['id'];
$no = 1;

if(@$_GET['action'] != 'kerjakansoal') { ?>
<!-- <div class="row">
    <div class="col-md-12">
        <h4 class="page-head-line">Ulangan</h4>
    </div>
</div> -->
<?php
}

if(@$_GET['action'] == '') { ?>

	<?php /*<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-heading">Data ulangan Setiap Mata Pelajaran</div>
	            <div class="panel-body">
	                <div class="table-responsive">
	                    <table class="table table-striped table-bordered table-hover">
	                        <thead>
	                            <tr>
	                                <th>#</th>
	                                <th>Mata Pelajaran</th>
	                                <th>Aksi</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                        <?php
	                        $sql_mapel = mysqli_query($db, "SELECT * FROM tb_mapel") or die ($db->error);
	                        while($data_mapel = mysqli_fetch_array($sql_mapel)) { ?>
	                            <tr>
	                                <td width="40px" align="center"><?php echo $no++; ?></td>
	                                <td><?php echo $data_mapel['mapel']; ?></td>
	                                <td width="200px" align="center">
	                                	<a href="?page=quiz&action=daftartopik&id_mapel=<?php echo $data_mapel['id']; ?>" class="btn btn-primary btn-xs">Lihat Soal</a>
	                                </td>
	                            </tr>
	                        	<?php
	                        } ?>
	                        </tbody>
	                    </table>
	                </div>
	            </div>
	        </div>
	    </div>
	</div>*/
	?>

	<div class="modal-dialog">
                <div class="loginmodal-container">
                    <img src="<?php echo 'assets/img/'.LOGO_SEKOLAH; ?>" width="170" class="center-block">
                    <h1>CBT Application</h1><br>
                 
                	<div class="table-responsive">
                		
	                    <table class="table table-striped table-bordered table-hover">
	                        <thead>
	                            <tr>
	                                <th>#</th>
	                                <th>Mata Pelajaran</th>
	                                <th>Jenis Ujian</th>
	                                <th>Aksi</th>
	                            </tr>
	                        </thead>
	                        <tbody>
	                       <?php 
                			$sql_tq = mysqli_query($db, "
                				SELECT tb_topik_quiz.*,tb_mapel.mapel,tb_mapel.id as mapel_id FROM tb_topik_quiz
                				LEFT JOIN tb_mapel ON tb_mapel.id = tb_topik_quiz.id_mapel
                				WHERE tb_topik_quiz.id_kelas = '$data_terlogin[id_kelas]' AND tb_topik_quiz.status = 'aktif'") or die ($db->error);
                			$sql_sudah_ujian = mysqli_query($db, "
                				SELECT id_tq from tb_nilai_pilgan
                				WHERE id_siswa = '$data_terlogin[id_siswa]'") or die ($db->error);
                			$list_sudah = mysqli_fetch_assoc($sql_sudah_ujian);
                			
                			//echo "<pre>"; print_r($list_sudah); exit;
							if(mysqli_num_rows($sql_tq) > 0) {
								while($data_tq = mysqli_fetch_array($sql_tq)) { ?>
	                            <tr>
	                                <td width="40px" align="center"><?php echo $no++; ?></td>
	                                <td><?php echo $data_tq['mapel']; ?></td>
	                                <td><?php echo $data_tq['judul']; ?></td>
	                                <td width="200px" align="center">
	                                	<?php if(!empty($list_sudah)){if (in_array($data_tq['id_tq'], $list_sudah)){?>
	                                		Sudah dikerjakan
	                                	<?php }else{?>
	                                	<a href="?page=quiz&action=daftartopik&id_mapel=<?php echo $data_tq['id_tq']; ?>" class="btn btn-primary btn-xs">Lihat Detail Ujian</a>
	                                	<?php }}else{?>
	                                	    <a href="?page=quiz&action=daftartopik&id_mapel=<?php echo $data_tq['id_tq']; ?>" class="btn btn-primary btn-xs">Lihat Detail Ujian</a>
	                                	<?php }?>
	                                </td>
	                            </tr>
	                        	<?php } }?>
	                        </tbody>
	                    </table>
	                </div>    
                </div>
            </div>

<?php
} else if(@$_GET['action'] == 'daftartopik') { ?>
	<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-heading">Data Tugas / Quiz Setiap Mata Pelajaran</div>
	            <div class="panel-body">
					<div class="table-responsive">
					<?php
					$id_mapel = @$_GET['id_mapel'];
					$sql_tq = mysqli_query($db, "SELECT * FROM tb_topik_quiz WHERE id_tq = '$id_mapel' AND id_kelas = '$data_terlogin[id_kelas]' AND status = 'aktif'") or die ($db->error);
					if(mysqli_num_rows($sql_tq) > 0) {
						while($data_tq = mysqli_fetch_array($sql_tq)) { ?>
						<table width="100%">
							<tr>
								<td valign="top">No. ( <?php echo $no++; ?> )</td>
								<td>
									<table class="table">
									    <thead>
									        <tr>
									            <td width="20%"><b>Judul</b></td>
									            <td>:</td>
									            <td width="65%"><?php echo $data_tq['judul']; ?></td>
									        </tr>
									    </thead>
									    <tbody>
									        <tr>
									            <td>Tanggal Pembuatan</td>
									            <td>:</td>
									            <td><?php echo tgl_indo($data_tq['tgl_buat']); ?></td>
									        </tr>
									        <tr>
									            <td>Pembuat</td>
									            <td>:</td>
									            <td>
									            	<?php
									            	if($data_tq['pembuat'] != 'admin') {
									            		$sql_peng = mysqli_query($db, "SELECT * FROM tb_pengajar WHERE id_pengajar = '$data_tq[pembuat]'") or die ($db->error);
									            		$data_peng = mysqli_fetch_array($sql_peng);
									            		echo $data_peng['nama_lengkap'];
									            	} else {
									            		echo $data_tq['pembuat'];
									            	} ?>
									            </td>
									        </tr>
									        <tr>
									            <td>Waktu Pengerjaan</td>
									            <td>:</td>
									            <td><?php echo $data_tq['waktu_soal'] / 60 ." menit"; ?></td>
									        </tr>
									        <tr>
									            <td>Info</td>
									            <td>:</td>
									            <td><?php echo $data_tq['info']; ?></td>
									        </tr>
									        <tr>
									        	<td></td>
									        	<td></td>
									        	<td>
									        		<a href="?page=quiz&action=infokerjakan&id_tq=<?php echo $data_tq['id_tq']; ?>" class="btn btn-primary btn-xs">Kerjakan Soal</a>
									        	</td>
									        </tr>
									    </tbody>
									</table>
								</td>
							</tr>
						</table>
						<?php
						}
					} else { ?>
						<div class="alert alert-danger">Data ulangan yang berada di kelas ini dengan mapel tersebut tidak ada</div>
						<?php
					} ?>
					</div>
	            </div>
	        </div>
	    </div>
	</div>
	<?php
} else if(@$_GET['action'] == 'infokerjakan') { ?>
	<div class="row">
	    <div class="col-md-12">
	        <div class="panel panel-default">
	            <div class="panel-body">
	            <?php
	            $sql_nilai = mysqli_query($db, "SELECT * FROM tb_nilai_pilgan WHERE id_tq = '$_GET[id_tq]' AND id_siswa = '$_SESSION[siswa]'") or die ($db->error);
	            $sql_jwb = mysqli_query($db, "SELECT * FROM tb_jawaban WHERE id_tq = '$_GET[id_tq]' AND id_siswa = '$_SESSION[siswa]'") or die ($db->error);
	            if(mysqli_num_rows($sql_nilai) > 0 || mysqli_num_rows($sql_jwb) > 0) {
	                $nilai = mysqli_fetch_array($sql_nilai,MYSQLI_ASSOC);
	            	echo "Anda sudah mengerjakan soal ini dengan nilai <b>". $nilai['presentase']."<b>";
	            } else { ?>
					1. Pastikan Anda sudah berada di wifi yang sudah ditentukan dan jangan berpindah wifi.<br />
					2. Pilih browser yang versi terbaru.<br />
					3. Baca dengan seksama dan teliti sebelum mengerjakan soal.<br />
					4. Jika terjadi mati lampu atau terputus koneksi wifi hubungi admin/proktor mata pelajaran terkait untuk melakukan ujian ulang.
					<?php
				} ?>
	            </div>
	            <div class="panel-footer">
					<?php
					if(mysqli_num_rows($sql_nilai) > 0 || mysqli_num_rows($sql_jwb) > 0) { ?>
						<a href="?page=quiz" class="btn btn-primary">Kembali</a>
						<?php
					} else {
						$sql_cek_soal_pilgan = mysqli_query($db, "SELECT * FROM tb_soal_pilgan WHERE id_tq = '$_GET[id_tq]'") or die ($db->error);
						$sql_cek_soal_essay = mysqli_query($db, "SELECT * FROM tb_soal_essay WHERE id_tq = '$_GET[id_tq]'") or die ($db->error);
						if(mysqli_num_rows($sql_cek_soal_pilgan) > 0 || mysqli_num_rows($sql_cek_soal_essay) > 0) { ?>
							<a href="soal.php?id_tq=<?php echo @$_GET['id_tq']; ?>" class="btn btn-primary">Mulai Mengerjakan</a>
						<?php
						} else { ?>
							<a onclick="alert('Data soal tidak ditemukan, mungkin karena belum dibuat. Silahkan hubungi guru yang bersangkutan');" class="btn btn-primary">Mulai Mengerjakan</a>
						<?php
						} ?>
						<a href="?page=quiz" class="btn btn-primary">Kembali</a>
					<?php
					} ?>
				</div>
	        </div>
	    </div>
	</div>
	<?php
} 
?>