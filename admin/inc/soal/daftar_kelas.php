<div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                  
                    <div class="panel-body">
                    	<div class="table-responsive">
                            <table class="table table-striped table-bordered table-hover" id="datasiswa">
                                <thead>
                                    <tr>
                                        <th>#</th>
                                        <th>Kelas</th>
                                        <th>Opsi</th>
                                    </tr>
                                </thead>
                                <tbody>
                                <?php

                                    $sql_siswa = mysqli_query($db, "SELECT * FROM tb_kelas ".$GLOBALS['limit']) or die ($db->error);
                                
                                if(mysqli_num_rows($sql_siswa) > 0) {
        	                        while($data_siswa = mysqli_fetch_array($sql_siswa)) { ?>
        	                            <tr>
        	                                <td align="center"><?php echo $no++; ?></td>
        	                                <td><?php echo $data_siswa['nama_kelas']; ?></td>
        	                                <td align="center">
                                                <a href="?page=quiz&IDkelas=<?php echo $data_siswa['id_kelas']; ?>" class="badge">Buka list Soal</a>
        	                                </td>
        	                            </tr>
        	                        <?php
        		                    }
        		                } else { ?>
        							<tr>
                                        <td colspan="8" align="center">Data tidak ditemukan</td>
        							</tr>
        		                	<?php
        		                } ?>
                                </tbody>
                            </table>
                            <script>
                            $(document).ready(function () {
                                $('#datasiswa').dataTable();
                            });
                            </script>
                        </div>
                    </div>
                </div>
        	</div>
        </div>