<?php /*
@session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Daftar CBT <?=NAMA_SEKOLAH;?></title>
    <link href="style/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="style/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="style/assets/css/style.css" rel="stylesheet" />
</head>
<body>
    <header>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    Anda sudah punya akun ? Silahkan <a href="./" class="btn btn-xs btn-danger">Login</a>
                </div>
            </div>
        </div>
    </header>
    <!-- HEADER END-->
    <div class="navbar navbar-inverse set-radius-zero">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="./">
                    <h4 style="color: white;"><?=NAMA_SEKOLAH;?></h4>
                    <h5 style="color: white;"><?=ALAMAT_SEKOLAH;?></h5>
                </a>

            </div>

            <div class="left-div">
                <div class="user-settings-wrapper">
                    <ul class="nav">
                        <li class="dropdown">
                            <a class="dropdown-toggle">
                                <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </div>
    </div>

    <section class="menu-section">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="navbar-collapse collapse ">
                        <ul id="menu-top" class="nav navbar-nav navbar-right">
                            <li><a <?php if(@$_GET['page'] == '') { echo 'class="menu-top-active"'; } ?> href="?hal=daftar">Register</a></li>
                            <li><a <?php if(@$_GET['page'] == 'berita') { echo 'class="menu-top-active"'; } ?> href="?hal=daftar&page=berita">Berita</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <div class="content-wrapper">
        <div class="container">
            <?php
            if(@$_GET['page'] == '') { ?>
                <div class="row">
                    <div class="col-md-12">
                        <h4 class="page-head-line">Halaman pendaftaran akun e-learning</h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-6">
                        <h4><i>Masukkan data Anda dengan benar !</i></h4>
                        <form method="post" enctype="multipart/form-data">
                            NIS* :<input type="text" name="nis" class="form-control" required />
                            Nama Lengkap* : <input type="text" name="nama_lengkap" class="form-control" required />
                            Tempat Lahir* : <input type="text" name="tempat_lahir" class="form-control" required />
                            Tanggal Lahir* : <input type="date" name="tgl_lahir" class="form-control" required />
                            Jenis Kelamin* :
                            <select name="jenis_kelamin" class="form-control" required>
                                <option value="">- Pilih -</option>
                                <option value="L">Laki-laki</option>
                                <option value="P">Perempuan</option>
                            </select>
                            Agama* :
                            <select name="agama" class="form-control" required>
                                <option value="">- Pilih -</option>
                                <option value="Islam">Islam</option>
                                <option value="Kristen">Kristen</option>
                                <option value="Katholik">Katholik</option>
                                <option value="Hindu">Hindu</option>
                                <option value="Budha">Budha</option>
                                <option value="Konghucu">Konghucu</option>
                            </select>
                            Nama Ayah* : <input type="text" name="nama_ayah" class="form-control" required />
                            Nama Ibu* : <input type="text" name="nama_ibu" class="form-control" required />
                            Nomor Telepon : <input type="text" name="no_telp" class="form-control" />
                            Email : <input type="email" name="email" class="form-control" />
                            Alamat* : <textarea name="alamat" class="form-control" rows="3" required></textarea>
                            Kelas* :
                            <select name="kelas" class="form-control" required>
                                <option value="">- Pilih -</option>
                                <?php
                                $sql_kelas = mysqli_query($db, "SELECT * from tb_kelas") or die ($db->error);
                                while($data_kelas = mysqli_fetch_array($sql_kelas)) {
                                    echo '<option value="'.$data_kelas['id_kelas'].'">'.$data_kelas['nama_kelas'].'</option>';
                                } ?>
                            </select>
                            Tahun Masuk* :
                            <select name="thn_masuk" class="form-control" required>
                                <option value="">- Pilih -</option>
                                <?php
                                for ($i = 2020; $i >= 2000; $i--) { 
                                    echo '<option value="'.$i.'">'.$i.'</option>';
                                } ?>
                            </select>
                            Foto : <input type="file" name="gambar" class="form-control" />
                            Username* : <input type="text" name="user" class="form-control" required />
                            Password* : <input type="password" name="pass" class="form-control" required />
                            <br />
                            <i><b>Catatan</b> : Tanda * wajib disi</i>
                            <hr />
                            <input type="submit" name="daftar" value="Daftar" class="btn btn-info" />
                            <input type="reset" class="btn btn-danger" />
                        </form>
                        <?php
                        if(@$_POST['daftar']) {
                            $nis = @mysqli_real_escape_string($db, $_POST['nis']);
                            $nama_lengkap = @mysqli_real_escape_string($db, $_POST['nama_lengkap']);
                            $tempat_lahir = @mysqli_real_escape_string($db, $_POST['tempat_lahir']);
                            $tgl_lahir = @mysqli_real_escape_string($db, $_POST['tgl_lahir']);
                            $jenis_kelamin = @mysqli_real_escape_string($db, $_POST['jenis_kelamin']);
                            $agama = @mysqli_real_escape_string($db, $_POST['agama']);
                            $nama_ayah = @mysqli_real_escape_string($db, $_POST['nama_ayah']);
                            $nama_ibu = @mysqli_real_escape_string($db, $_POST['nama_ibu']);
                            $no_telp = @mysqli_real_escape_string($db, $_POST['no_telp']);
                            $email = @mysqli_real_escape_string($db, $_POST['email']);
                            $alamat = @mysqli_real_escape_string($db, $_POST['alamat']);
                            $kelas = @mysqli_real_escape_string($db, $_POST['kelas']);
                            $thn_masuk = @mysqli_real_escape_string($db, $_POST['thn_masuk']);
                            $user = @mysqli_real_escape_string($db, $_POST['user']);
                            $pass = @mysqli_real_escape_string($db, $_POST['pass']);
                            $pass = md5($pass);

                            $sumber = @$_FILES['gambar']['tmp_name'];
                            $target = DIR_ASSETS.'img/foto_siswa/';
                            $nama_gambar = @$_FILES['gambar']['name'];

                            $sql_cek_user = mysqli_query($db, "SELECT * FROM tb_siswa WHERE username = '$user'") or die ($db->error);
                            if(mysqli_num_rows($sql_cek_user) > 0) {
                                echo "<script>alert('Username yang Anda pilih sudah ada, silahkan ganti yang lain');</script>";
                            } else {
                                if($nama_gambar != '') {
                                    if(move_uploaded_file($sumber, $target.md5($nis.$nama_gambar).'.'.pathinfo($nama_gambar, PATHINFO_EXTENSION))) {
                                        $nama_gambar = md5($nis.$nama_gambar).'.'.pathinfo($nama_gambar, PATHINFO_EXTENSION); 
                                        $sql = "INSERT INTO tb_siswa(
                                                                nis,
                                                                nama_lengkap,
                                                                tgl_lahir,
                                                                jenis_kelamin,
                                                                agama,
                                                                nama_ayah,
                                                                nama_ibu,
                                                                no_telp,
                                                                email,
                                                                alamat,
                                                                id_kelas,
                                                                thn_masuk,
                                                                foto,
                                                                username,
                                                                password,
                                                                pass,
                                                                status,
                                                                tempat_lahir)
                                                             VALUES(
                                                                '$nis', 
                                                                '$nama_lengkap', 
                                                                '$tgl_lahir', 
                                                                '$jenis_kelamin', 
                                                                '$agama', 
                                                                '$nama_ayah', 
                                                                '$nama_ibu', 
                                                                '$no_telp', 
                                                                '$email', 
                                                                '$alamat', 
                                                                '$kelas', 
                                                                '$thn_masuk', 
                                                                '$nama_gambar', 
                                                                '$user', 
                                                                '$pass', 
                                                                '$_POST[pass]', 
                                                                'tidak aktif',
                                                                '$tempat_lahir')";
                                        $r = mysqli_query($db, $sql) or die ($db->error);          
                                        if($r){echo '<script>alert("Pendaftaran berhasil, silahkan login"); window.location="./"</script>';}
                                        else echo '<script>alert("terjadi kesalahan");'; 
                                    } else {
                                        echo '<script>alert("Gagal mendaftar, foto gagal diupload, coba lagi!");</script>';
                                    }
                                } else {
                                    mysqli_query($db, "INSERT INTO tb_siswa(
                                                                    nis,
                                                                nama_lengkap,
                                                                tgl_lahir,
                                                                jenis_kelamin,
                                                                agama,
                                                                nama_ayah,
                                                                nama_ibu,
                                                                no_telp,
                                                                email,
                                                                alamat,
                                                                id_kelas,
                                                                thn_masuk,
                                                                foto,
                                                                username,
                                                                password,
                                                                pass,
                                                                status,
                                                                tempat_lahir) 
                                                                VALUES(
                                                                '$nis', 
                                                                '$nama_lengkap', 
                                                                '$tgl_lahir', 
                                                                '$jenis_kelamin', 
                                                                '$agama', 
                                                                '$nama_ayah', 
                                                                '$nama_ibu', 
                                                                '$no_telp', 
                                                                '$email', 
                                                                '$alamat', 
                                                                '$kelas', 
                                                                '$thn_masuk', 
                                                                'anonim.png', 
                                                                '$user', 
                                                                '$pass', 
                                                                '$_POST[pass]', 
                                                                'tidak aktif',
                                                                '$tempat_lahir'
                                                        )") or die ($db->error);          
                                    echo '<script>alert("Pendaftaran berhasil, tunggu akun aktif dan silahkan login"); window.location="./"</script>';
                                }
                            }
                        }
                        ?>
                    </div>
                    <div class="col-md-6">
                        <div class="alert alert-warning">
                            Untuk menggunakan layanan e-learning ini kalian harus memiliki akun terlebih dahulu.
                        </div>
                    </div>
                </div>
            <?php
            } else if(@$_GET['page'] == 'berita') {
                include "inc/berita.php";
            } ?>
        </div>
    </div>
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; <?=date('Y');?> CBT <?=NAMA_SEKOLAH;?> | By : <?=COMPANY;?>
                </div>

            </div>
        </div>
    </footer>
    <script src="style/assets/js/jquery-1.11.1.js"></script>
    <script src="style/assets/js/bootstrap.js"></script>
</body>
</html>
*/?>

<?php
@session_start();
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Login CBT <?=NAMA_SEKOLAH;?></title>
    <link href="style/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="style/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="style/assets/css/style.css" rel="stylesheet" />
</head>
<body>

    <div class="content-wrapper">
        <div class="container">
            <?php
            if(@$_GET['page'] == '') { ?>
                <div class="row">
                </div>
               
<style type="text/css">
.content-wrapper{
  padding: 0px;
  margin-top: 0px;
}
body{
    background: green;
}
    /****** form MODAL ******/
        .modal-dialog{
            width: 80%;
        }        
.loginmodal-container {
  padding: 30px;
  /*max-width: 650px;*/
  width: 80% !important;
  background-color: white;
  margin: 0 auto;
  border-radius: 2px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  overflow: hidden;
  font-family: roboto;
}

.loginmodal-container h1 {
  text-align: center;
  font-size: 1.8em;
  font-family: roboto;
}

.loginmodal-container input[type=submit] {
  width: 100%;
  display: block;
  margin-bottom: 10px;
  position: relative;
}

.loginmodal-container input[type=text], input[type=password] {
  height: 44px;
  font-size: 16px;
  width: 100%;
 /* margin-bottom: 10px;*/
  -webkit-appearance: none;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-top: 1px solid #c0c0c0;
  /* border-radius: 2px; */
  padding: 0 8px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.loginmodal-container input[type=text]:hover, input[type=password]:hover {
  border: 1px solid #b9b9b9;
  border-top: 1px solid #a0a0a0;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}

.loginmodal {
  text-align: center;
  font-size: 14px;
  font-family: 'Arial', sans-serif;
  font-weight: 700;
  height: 36px;
  padding: 0 8px;
/* border-radius: 3px; */
/* -webkit-user-select: none;
  user-select: none; */
}

.loginmodal-submit {
  /* border: 1px solid #3079ed; */
  border: 0px;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.1); 
  background-color: #4d90fe;
  padding: 17px 0px;
  font-family: roboto;
  font-size: 14px;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
}

.loginmodal-submit:hover {
  /* border: 1px solid #2f5bb7; */
  border: 0px;
  text-shadow: 0 1px rgba(0,0,0,0.3);
  background-color: #357ae8;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
}

.loginmodal-container a {
  text-decoration: none;
  color: #666;
  font-weight: 400;
  text-align: center;
  display: inline-block;
  opacity: 0.6;
  transition: opacity ease 0.5s;
} 

.login-help{
  font-size: 16px;
  text-align: center;
}
                </style>
                <div class="modal-dialog">
                    
                <div class="loginmodal-container">
                    <img src="<?php echo URL_ASSETS."img/".LOGO_SEKOLAH; ?>" width="170" class="center-block">
                    <h1>CBT Application</h1><br>
                    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
                      <form class="form-horizontal" method="post" enctype="multipart/form-data">
                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">NIS</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-address-card fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="nis" id="nis"  placeholder="Ketikan Nomor Induk Siswa"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Nama Lengkap</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-user fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="nama_lengkap" id="name"  placeholder="Ketikan Nama Lengkap"/>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Kelas</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    
                        <select name="kelas" class="form-control" required>
                                <option value="">- Pilih -</option>
                                <?php
                                $sql_kelas = mysqli_query($db, "SELECT * from tb_kelas") or die ($db->error);
                                while($data_kelas = mysqli_fetch_array($sql_kelas)) {
                                    echo '<option value="'.$data_kelas['id_kelas'].'">'.$data_kelas['nama_kelas'].'</option>';
                                } ?>
                            </select>
                              </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Jenis Kelamin</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-venus-mars fa" aria-hidden="true"></i></span>
                                    <select name="jenis_kelamin" class="form-control" required>
                                        <option value="">- Pilih -</option>
                                        <option value="L">Laki-laki</option>
                                        <option value="P">Perempuan</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                         <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Agama</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-region fa" aria-hidden="true"></i></span>
                                    <select name="agama" class="form-control" required>
                                        <option value="">- Pilih -</option>
                                        <option value="Islam">Islam</option>
                                        <option value="Kristen">Kristen</option>
                                        <option value="Katholik">Katholik</option>
                                        <option value="Hindu">Hindu</option>
                                        <option value="Budha">Budha</option>
                                        <option value="Konghucu">Konghucu</option>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Tanggal Lahir</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-birthday-cake  fa" aria-hidden="true"></i></span>
                                    <input type="date" class="form-control" name="tgl_lahir" id="tanggallahir"  placeholder="Pilih Tanggal Lahir"/>
                                </div>
                            </div>
                        </div>

                         <div class="form-group">
                            <label for="name" class="cols-sm-2 control-label">Tahun Masuk Sekolah</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-calendar fa" aria-hidden="true"></i></span>
                                    <select name="thn_masuk" class="form-control" required>
                                        <option value="">- Pilih -</option>
                                        <?php
                                        for ($i = 2020; $i >= 2000; $i--) { 
                                            echo '<option value="'.$i.'">'.$i.'</option>';
                                        } ?>
                                    </select>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="cols-sm-2 control-label">Email</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-envelope fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="email" id="email"  placeholder="Ketikan Alamat Email"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="cols-sm-2 control-label">Telp</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-mobile fa" aria-hidden="true"></i></span>
                                    <input type="text" name="no_telp" class="form-control"  placeholder="Ketikan No Hp"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="email" class="cols-sm-2 control-label">Foto</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-file-image-o fa" aria-hidden="true"></i></span>
                                    <input type="file" class="form-control" name="gambar" id="foto"  placeholder="Enter your Email"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="username" class="cols-sm-2 control-label">Username</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-users fa" aria-hidden="true"></i></span>
                                    <input type="text" class="form-control" name="user" id="username"  placeholder="Enter your Username"/>
                                </div>
                            </div>
                        </div>

                        <div class="form-group">
                            <label for="password" class="cols-sm-2 control-label">Password</label>
                            <div class="cols-sm-10">
                                <div class="input-group">
                                    <span class="input-group-addon"><i class="fa fa-lock fa-lg" aria-hidden="true"></i></span>
                                    <input type="password" class="form-control" name="pass" id="password"  placeholder="Enter your Password"/>
                                </div>
                            </div>
                        </div>


                        <div class="form-group ">
                            <input type="submit" class="btn btn-primary btn-lg btn-block login-button" value="Register" name="daftar">
                        </div>
                        <div class="login-register">
                            <a href="index.php">Login</a>
                         </div>
                    </form>
                    <?php
                        if(@$_POST['daftar']) {
                            $nis = @mysqli_real_escape_string($db, $_POST['nis']);
                            $nama_lengkap = @mysqli_real_escape_string($db, $_POST['nama_lengkap']);
                            // $tempat_lahir = @mysqli_real_escape_string($db, $_POST['tempat_lahir']);
                            $tgl_lahir = @mysqli_real_escape_string($db, $_POST['tgl_lahir']);
                            $jenis_kelamin = @mysqli_real_escape_string($db, $_POST['jenis_kelamin']);
                            $agama = @mysqli_real_escape_string($db, $_POST['agama']);
                            // $nama_ayah = @mysqli_real_escape_string($db, $_POST['nama_ayah']);
                            // $nama_ibu = @mysqli_real_escape_string($db, $_POST['nama_ibu']);
                            $no_telp = @mysqli_real_escape_string($db, $_POST['no_telp']);
                            $email = @mysqli_real_escape_string($db, $_POST['email']);
                            $alamat = @mysqli_real_escape_string($db, $_POST['alamat']);
                            $kelas = @mysqli_real_escape_string($db, $_POST['kelas']);
                            $thn_masuk = @mysqli_real_escape_string($db, $_POST['thn_masuk']);
                            $user = @mysqli_real_escape_string($db, $_POST['user']);
                            $pass = @mysqli_real_escape_string($db, $_POST['pass']);
                            $pass = md5($pass);

                            $sumber = @$_FILES['gambar']['tmp_name'];
                            $target = DIR_ASSETS.'img/foto_siswa/';
                            $nama_gambar = @$_FILES['gambar']['name'];

                            $sql_cek_user = mysqli_query($db, "SELECT * FROM tb_siswa WHERE username = '$user'") or die ($db->error);
                            if(mysqli_num_rows($sql_cek_user) > 0) {
                                echo "<script>alert('Username yang Anda pilih sudah ada, silahkan ganti yang lain');</script>";
                            } else {
                                if($nama_gambar != '') {
                                    if(move_uploaded_file($sumber, $target.md5($nis.$nama_gambar).'.'.pathinfo($nama_gambar, PATHINFO_EXTENSION))) {
                                        $nama_gambar = md5($nis.$nama_gambar).'.'.pathinfo($nama_gambar, PATHINFO_EXTENSION); 
                                        $sql = "INSERT INTO tb_siswa(
                                                                nis,
                                                                nama_lengkap,
                                                                tgl_lahir,
                                                                jenis_kelamin,
                                                                agama,
                                                                no_telp,
                                                                email,
                                                                id_kelas,
                                                                thn_masuk,
                                                                foto,
                                                                username,
                                                                password,
                                                                pass,
                                                                status
                                                                )
                                                             VALUES(
                                                                '$nis', 
                                                                '$nama_lengkap',
                                                                '$tgl_lahir', 
                                                            '$jenis_kelamin', 
                                                                '$agama', 
                                                                '$no_telp', 
                                                                '$email', 
                                                                '$thn_masuk', 
                                                                '$nama_gambar', 
                                                                '$user', 
                                                                '$pass', 
                                                                '".$_POST['pass']."', 
                                                                'tidak aktif'
                                                                )";
                                        $r = mysqli_query($db, $sql) or die ($db->error);          
                                        if($r){echo '<script>alert("Pendaftaran berhasil, silahkan login"); window.location="./"</script>';}
                                        else echo '<script>alert("terjadi kesalahan");'; 
                                    } else {
                                        echo '<script>alert("Gagal mendaftar, foto gagal diupload, coba lagi!");</script>';
                                    }
                                } else {
                                    mysqli_query($db, "INSERT INTO tb_siswa(
                                                                    nis,
                                                                nama_lengkap,
                                                                tgl_lahir,
                                                                jenis_kelamin,
                                                                agama,
                                                                no_telp,
                                                                email,
                                                                id_kelas,
                                                                thn_masuk,
                                                                foto,
                                                                username,
                                                                password,
                                                                pass,
                                                                status
                                                                ) 
                                                                VALUES(
                                                                '$nis', 
                                                                '$nama_lengkap',
                                                                '$tgl_lahir', 
                                                                '$jenis_kelamin', 
                                                                '$agama', 
                                                                '$no_telp', 
                                                                '$email', 
                                                                '$kelas', 
                                                                '$thn_masuk', 
                                                                'anonim.png', 
                                                                '$user', 
                                                                '$pass', 
                                                                '$_POST[pass]', 
                                                                'tidak aktif'
                                                        )") or die ($db->error);          
                                    echo '<script>alert("Pendaftaran berhasil, tunggu akun aktif dan silahkan login"); window.location="./index.php"</script>';
                                }
                            }
                        }
                    }
                        ?>
                </div>
            </div>
        </div>
    </div>

    <footer style="background: blue; position: fixed; bottom: 0px; right: 0px; width: 100%;">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    &copy; <?=date('Y');?> CBT Software | By : <?=COMPANY;?>
                </div>

            </div>
        </div>
    </footer>
    <script  src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
    <script src="style/assets/js/bootstrap.js"></script>
</body>
</html>
