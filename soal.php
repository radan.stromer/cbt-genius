    <?php
@session_start();
include "+koneksi.php";

$id_tq = @$_GET['id_tq'];
$no = 1;
$no2 = 1;
$sql_tq = mysqli_query($db, "SELECT * FROM tb_topik_quiz JOIN tb_mapel ON tb_topik_quiz.id_mapel = tb_mapel.id WHERE id_tq = '$id_tq'") or die ($db->error);
$data_tq = mysqli_fetch_array($sql_tq);
?>
<script  src="https://code.jquery.com/jquery-1.12.4.min.js" integrity="sha256-ZosEbRLbNQzLpnKIkEdrPv7lOy9C27hHQ+Xp8a4MxAQ=" crossorigin="anonymous"></script>
<script src="style/assets/js/bootstrap.js"></script>
<script src="assets/js/jquery.magnific-popup.js"></script>
<script>
var waktunya;
waktunya = <?php echo $data_tq['waktu_soal']; ?>;
var waktu;
var jalan = 0;
var habis = 0;

function init(){
    checkCookie();
    mulai();
}
function keluar(){
    if(habis==0){
        setCookie('waktux',waktu,365);
    }else{
        setCookie('waktux',0,-1);
    }
}
function mulai(){
    jam = Math.floor(waktu/3600);
    sisa = waktu%3600;
    menit = Math.floor(sisa/60);
    sisa2 = sisa%60
    detik = sisa2%60;
    if(detik<10){
        detikx = "0"+detik;
    }else{
        detikx = detik;
    }
    if(menit<10){
        menitx = "0"+menit;
    }else{
        menitx = menit;
    }
    if(jam<10){
        jamx = "0"+jam;
    }else{
        jamx = jam;
    }
    document.getElementById("divwaktu").innerHTML = jamx+": "+menitx+":"+detikx;
    waktu --;
    if(waktu>0){
        t = setTimeout("mulai()",1000);
        jalan = 1;
    }else{
        if(jalan==1){
            clearTimeout(t);
        }
        habis = 1;
        document.getElementById("kirim").click();
        //selesai();
    }
}
function selesai(){    
    if(jalan==1){
        clearTimeout(t);
    }
    habis = 1;
}
function getCookie(c_name){
    if (document.cookie.length>0){
        c_start=document.cookie.indexOf(c_name + "=");
        if (c_start!=-1){
            c_start=c_start + c_name.length+1;
            c_end=document.cookie.indexOf(";",c_start);
            if (c_end==-1) c_end=document.cookie.length;
            return unescape(document.cookie.substring(c_start,c_end));
        }
    }
    return "";
}
function setCookie(c_name,value,expiredays){
    var exdate=new Date();
    exdate.setDate(exdate.getDate()+expiredays);
    document.cookie=c_name+ "=" +escape(value)+((expiredays==null) ? "" : ";expires="+exdate.toGMTString());
}
function checkCookie(){
    waktuy=getCookie('waktux');
    if (waktuy!=null && waktuy!=""){
        waktu = waktuy;
    }else{
        waktu = waktunya;
        setCookie('waktux',waktunya,7);
    }
}
</script>
<script type="text/javascript">
    window.history.forward();
    function noBack(){ window.history.forward(); }
</script>

<?php
if(@$_SESSION['siswa']) { ?>

<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />
    <title>Ujian Online E-Learning <?=NAMA_SEKOLAH;?></title>
    <link href="style/assets/css/bootstrap.css" rel="stylesheet" />
    <link href="style/assets/css/font-awesome.css" rel="stylesheet" />
    <link href="style/assets/css/style.css" rel="stylesheet" />
    <link href="assets/css/magnific-popup.css" rel="stylesheet" />
    <style type="text/css">
    .mrg-del {
        margin: 0;
        padding: 0;
    }
    </style>
</head>
<body onload="init(),noBack();" onpageshow="if (event.persisted) noBack();" onunload="keluar()">
    <style type="text/css">
               .content-wrapper{
  padding: 0px;
  margin-top: 0px;
}
body{
    background: green;
}
    /****** form MODAL ******/
        .modal-dialog{
            width: 80%;
        }

.konten-ujian{
    background: white;
}        
.loginmodal-container {
  padding: 30px;
  /*max-width: 650px;*/
  width: 80% !important;
  background-color: white;
  margin: 0 auto;
  border-radius: 2px;
  box-shadow: 0px 2px 2px rgba(0, 0, 0, 0.3);
  overflow: hidden;
  font-family: roboto;
}

.loginmodal-container h1 {
  text-align: center;
  font-size: 1.8em;
  font-family: roboto;
}

.loginmodal-container input[type=submit] {
  width: 100%;
  display: block;
  margin-bottom: 10px;
  position: relative;
}

.loginmodal-container input[type=text], input[type=password] {
  height: 44px;
  font-size: 16px;
  width: 100%;
 /* margin-bottom: 10px;*/
  -webkit-appearance: none;
  background: #fff;
  border: 1px solid #d9d9d9;
  border-top: 1px solid #c0c0c0;
  /* border-radius: 2px; */
  padding: 0 8px;
  box-sizing: border-box;
  -moz-box-sizing: border-box;
}

.loginmodal-container input[type=text]:hover, input[type=password]:hover {
  border: 1px solid #b9b9b9;
  border-top: 1px solid #a0a0a0;
  -moz-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  -webkit-box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
  box-shadow: inset 0 1px 2px rgba(0,0,0,0.1);
}

.loginmodal {
  text-align: center;
  font-size: 14px;
  font-family: 'Arial', sans-serif;
  font-weight: 700;
  height: 36px;
  padding: 0 8px;
/* border-radius: 3px; */
/* -webkit-user-select: none;
  user-select: none; */
}

.loginmodal-submit {
  /* border: 1px solid #3079ed; */
  border: 0px;
  color: #fff;
  text-shadow: 0 1px rgba(0,0,0,0.1); 
  background-color: #4d90fe;
  padding: 17px 0px;
  font-family: roboto;
  font-size: 14px;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#4787ed)); */
}

.loginmodal-submit:hover {
  /* border: 1px solid #2f5bb7; */
  border: 0px;
  text-shadow: 0 1px rgba(0,0,0,0.3);
  background-color: #357ae8;
  /* background-image: -webkit-gradient(linear, 0 0, 0 100%,   from(#4d90fe), to(#357ae8)); */
}

.loginmodal-container a {
  text-decoration: none;
  font-weight: 400;
  text-align: center;
  display: inline-block;
  opacity: 0.6;
  transition: opacity ease 0.5s;
} 

.login-help{
  font-size: 16px;
  text-align: center;
}

                </style>

<!-- <div class="navbar navbar-inverse set-radius-zero">
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="./">
                <h4 style="color: white;"><?=NAMA_SEKOLAH;?></h4>
                    <h5 style="color: white;"><?=ALAMAT_SEKOLAH;?></h5>
            </a>
        </div>

        <div class="left-div">
            <div class="user-settings-wrapper">
                <ul class="nav">
                    <li class="dropdown">
                        <a class="dropdown-toggle" data-toggle="dropdown" href="#" aria-expanded="false">
                            <span class="glyphicon glyphicon-user" style="font-size: 25px;"></span>
                        </a>
                    </li>
                </ul>
            </div>
        </div>
    </div>
</div> -->

<div class="content-wrapper">
    <div class="container">
		<div class="row">
		    <div class="col-md-9">
                <div class="row">
                    <img class="col-md-2" src="<?php echo 'assets/img/'.LOGO_SEKOLAH; ?>" width="140" style="float: left;">
                    <div class="col-md-10">
                        <h2 style="color: white"><?=NAMA_SEKOLAH;?></h2>
                        <h3 style="margin-top:10px;color: white; float: left;">CBT Application</h3>
                        <h4 style="margin-top:10px;float:right;font-weight: 900; text-transform: uppercase; color: yellow;font-size: 20px;">
                            <span>Mapel : <?php echo $data_tq['mapel']; ?></span>
                        </h4>
                    </div>
                </div>
		    </div>
             <div class="col-md-3">
                 <div class="panel-body">
                    <span style="margin-top:25px;float:left;background: yellow; padding: 10px; margin-right: 0px; font-weight: 900; text-transform: uppercase;font-size: 18px;">Sisa Waktu</span>
                    <span  style="margin-top:25px;float:left;background: red; padding: 10px;font-weight: 900; text-transform: uppercase;font-size: 18px; margin-left: 0px;" id="divwaktu"></span>
                </div>   
            </div>
		</div>

		<div class="row">
            <div class="col-md-9">
                <?php include "kanan_soal.php";?>
            </div>
            <div class="col-md-3 lembar-jawaban" style="padding-left:  2px; padding-right: 0px;">
            
               
		       
                <?php $sql_soal_sudah_jawab = mysqli_query($db, "SELECT 
                    tb_jawaban_pilgan_temp.id_soal,
                    tb_jawaban_pilgan_temp.jawaban
                    FROM tb_jawaban_pilgan_temp 
                    #LEFT JOIN tb_soal_pilgan ON tb_soal_pilgan.id_pilgan = tb_jawaban_pilgan_temp.id_soal
                    WHERE id_peserta = '".$_SESSION['siswa']."' AND id_tq = '{$id_tq}'") or die ($db->error);
                    $soal_sudah_jawab = mysqli_num_rows($sql_soal_sudah_jawab);
                    if($soal_sudah_jawab>0) { 
                ?>
                <!-- <div class="panel panel-default">
                    <div class="panel-heading"><b>Butir Soal</small></b></div>
                    <div class="panel-body">
                        <div class="list-group">
                            <?php $no=1; foreach($sql_soal_sudah_jawab as $soal){?>
                                <a href="soal.php?id_tq=<?php echo $id_tq;?>&revisi_soal=<?php echo $soal['id_soal'];?>&no_revisi=<?php echo $no;?>" class="list-group-item"><?php echo $no.")".$soal['jawaban'];?></a>
                            <?php $no++;}?>
                        </div>
                    </div>
                </div> -->
                <style type="text/css">
                    /*Mengatur lebar tombol nomor soal*/
                    .blok-nomor{
                       width: 20%;
                       display: inline-block;
                    }
                    .blok-nomor .box{
                       padding: 5px;
                       
                    }

                    /*Mengatur desain tombol nomor soal*/
                    .tombol-nomor{
                       display: block;
                       width: 100%;
                       text-align: center;
                       background: red;
                       color: #fff;
                       border: 2px solid #000;
                       cursor: pointer;
                       font-size: 16px;
                    }
                    /*Mengatur warna tombol nomor soal*/
                    .tombol-nomor.green{
                        background: black;
                    }
                    .tombol-nomor.yellow{
                        background: red;
                        color: white;
                    }
                    .nomor-ujian{
                        border: none;
                    }

                    
                </style>
                <div class="nomor-ujian">
                    <?php $no=1; foreach($sql_soal_sudah_jawab as $soal){ 
                        if(!empty($soal['jawaban'])) $warna_tombol = "tombol-nomor green";
                        else $warna_tombol = "tombol-nomor yellow";
                    ?>
                    <div class="blok-nomor">
                        <div class="box" style="position:relative;">
                            <a class="tombol-nomor <?=$warna_tombol;?>" href="soal.php?id_tq=<?php echo $id_tq;?>&revisi_soal=<?php echo $soal['id_soal'];?>&no_revisi=<?php echo $no;?>">
                                <?php echo $no;?>
                                <span style="position: absolute; top:-3px; font-weight: 0; padding: 6px 9px; background: white; color: black;" class="badge"><?php echo $soal['jawaban']; ?></span>
                            </a>
                        </div>
                    </div>
                    <?php  $no++; }?>
                </div>
                <?php }?>
		    </div>

		    
		</div>

	</div>
</div>

<footer style="background: blue; position: fixed; bottom: 0px; right: 0px; width: 100%;">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                &copy; <?=date('Y');?> CBT Software | By : <?=COMPANY;?>
            </div>
        </div>
    </div>
</footer>

<script type="text/javascript">
    $('.img-soal').magnificPopup({
        type: 'image',
        closeOnContentClick: true,
        closeBtnInside: false,
        fixedContentPos: true,
        mainClass: 'mfp-no-margins mfp-with-zoom', // class to remove default margin from left and right side
        image: {
            verticalFit: true
        },
        zoom: {
            enabled: true,
            duration: 300 // don't foget to change the duration also in CSS
        }
    });

    $("#hide-answer").click(function(){
        $(".nomor-ujian").stop().toggle('slide');
    });
</script>
</body>
</html>
<?php } else {
	echo "<script>window.location='./';</script>";
}?>